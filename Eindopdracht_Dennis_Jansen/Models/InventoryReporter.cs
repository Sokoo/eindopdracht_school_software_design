﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Eindopdracht_Dennis_Jansen.Models
{
    public class InventoryReporter : IObserver<InventoryItem>
    {
        private IDisposable unsubscriber;
        private bool first = true;
        private InventoryItem last;

        public virtual void Subscribe(IObservable<InventoryItem> provider)
        {
            unsubscriber = provider.Subscribe(this);
        }

        public virtual void Unsubscribe()
        {
            unsubscriber.Dispose();
        }

        public void OnNext(InventoryItem value)
        {
            MessageBox.Show($"{value.Title} is toegevoegd aan je inventory, met een waarde van {value.Price}");
        }

        public void OnError(Exception error)
        {
            throw new NotImplementedException();
        }

        public void OnCompleted()
        {
            throw new NotImplementedException();
        }
    }
}
