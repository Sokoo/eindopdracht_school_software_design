﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eindopdracht_Dennis_Jansen.Models
{
    public class ObservableInventory : IObservable<InventoryItem>
    {
        public List<IObserver<InventoryItem>> Observers { get; set; }
        public ObservableCollection<InventoryItem> Items { get; set; }
        public int ObsCounter = 1;
        public int ItemCounter = 1;

        public ObservableInventory()
        {
            Observers = new List<IObserver<InventoryItem>>();
            Items = new ObservableCollection<InventoryItem>();
            Init();
        }

        void Init()
        {
            var item = new InventoryItem
            {
                Id = ItemCounter,
                Description = "Test1",
                Title = "Title test 1",
                Price = "€458,52"
            };
            Items.Add(item);
            ItemCounter++;
        }

        public void ItemStatus(InventoryItem item)
        {
            item.Id = ItemCounter;
            ItemCounter++;
            if (!Items.Contains(item))
            {
                Items.Add(item);
                foreach (var observer in Observers)
                {
                    observer.OnNext(item);
                }
            }
        }

        public IDisposable Subscribe(IObserver<InventoryItem> observer)
        {
            if (!Observers.Contains(observer))
            {
                Observers.Add(observer);
                foreach (var item in Items)
                {
                    observer.OnNext(item);
                }
            }
            return new Unsubscriber<InventoryItem>(Observers, observer);
        }
    }
}
