﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eindopdracht_Dennis_Jansen.Models
{
    internal class Unsubscriber<InventoryItem> : IDisposable
    {
        private List<IObserver<InventoryItem>> _observers;
        private IObserver<InventoryItem> _observer;

        internal Unsubscriber(List<IObserver<InventoryItem>> observers, IObserver<InventoryItem> observer)
        {
            this._observers = observers;
            this._observer = observer;
        }

        public void Dispose()
        {
            if (_observers.Contains(_observer))
                _observers.Remove(_observer);
        }
    }
}
