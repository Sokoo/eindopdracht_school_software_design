﻿using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Eindopdracht_Dennis_Jansen.Models;

namespace Eindopdracht_Dennis_Jansen
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ObservableInventory Provider = new ObservableInventory();
        InventoryReporter Observer = new InventoryReporter();
        
        public MainWindow()
        {
            InitializeComponent();
            Title = "Eindopdracht Dennis Jansen";
            LblObsCount.Content = Provider.ObsCounter;
            DataContext = Provider;
        }

        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            var newItem = new InventoryItem
            {
                Title = TbTitle.Text,
                Description = TbDescr.Text,
                Price = "€" + TbPrice.Text
            };

            Provider.ItemStatus(newItem);
            Observer.Subscribe(Provider);
            //Provider.Items.Add(newItem);
            ListViewInventory.ItemsSource = Provider.Items;
            newItem = null;
        }

        private void TbPrice_OnPreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsValid(((TextBox)sender).Text + e.Text);
        }

        public static bool IsValid(string str)
        {
            int i;
            return int.TryParse(str, out i) && i >= 1 && i <= 9999;
        }

        private void BtnNewObs_Click(object sender, RoutedEventArgs e)
        {
            var newObs = new InventoryReporter();
            Provider.ObsCounter++;
            LblObsCount.Content = Provider.ObsCounter;
            newObs.Subscribe(Provider);
        }

        private void ListViewInventory_OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var selectedItem = ListViewInventory.SelectedItems[0] as InventoryItem;
            EditItemWindow edt = new EditItemWindow(selectedItem, Provider);
            edt.Show();
        }
    }
}
