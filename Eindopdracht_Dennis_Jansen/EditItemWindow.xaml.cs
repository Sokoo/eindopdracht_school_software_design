﻿using System.Linq;
using System.Windows;
using Eindopdracht_Dennis_Jansen.Models;

namespace Eindopdracht_Dennis_Jansen
{
    /// <summary>
    /// Interaction logic for EditItemWindow.xaml
    /// </summary>
    public partial class EditItemWindow : Window
    {
        private readonly ObservableInventory _prvd;
        private InventoryItem _item { get; set; }

        public EditItemWindow(InventoryItem item, ObservableInventory prvd)
        {
            _prvd = prvd;
            _item = item;
            InitializeComponent();

            Title = "Muteren inventory items";
            TbTitle.Text = _item.Title;
            TbDesc.Text = _item.Description;
            TbPrice.Text = _item.Price;
        }

        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            var newItem = new InventoryItem
            {
                Id = _item.Id,
                Title = TbTitle.Text,
                Description = TbDesc.Text,
                Price = TbPrice.Text
            };
            _prvd.Items.Remove(_prvd.Items.Single(c => c.Id == _item.Id));
            _prvd.ItemStatus(newItem);
            this.Close();
        }

        private void BtnDelete_Click(object sender, RoutedEventArgs e)
        {
            _prvd.Items.Remove(_prvd.Items.Single(c => c.Id == _item.Id));
            this.Close();
        }
    }
}
